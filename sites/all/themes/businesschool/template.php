<?php


/**
 * Helper to rendered block().
 */
function businesschool_get_rendered_block($module_name, $block_name) {
  $block_render_data = block_load($module_name, $block_name);
  $block_render_data = _block_render_blocks(array($block_render_data));
  $block_build = _block_get_renderable_array($block_render_data);
  $rendered_block = drupal_render($block_build);
  return $rendered_block;
}

/**
 * Override theme_breadcrumb().
 */
function businesschool_breadcrumb($variables) {
  $breadcrumb = $variables ['breadcrumb'];

  if (!empty($breadcrumb)) { 

    if (current_path() == 'contacts') {
      $breadcrumb[0] = l('Главная', '/');
      $breadcrumb[1] = l('О компании', 'node/97');
      $breadcrumb[2] = 'Контакты'; 
    }

		if (request_uri() == '/calendar') {
      $breadcrumb[0] = l('Главная', '/');
      $breadcrumb[1] = l('Программы', 'programs');
      $breadcrumb[2] = 'Календарь'; 
    }  
	
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode('<span class="sep"> / </span>', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Override theme_textarea().
 */
function businesschool_textarea($vars) {
  $element = $vars['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
  _form_set_class($element, array('form-textarea'));
  return '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
}

function businesschool_preprocess_html(&$variables) {
  // Add variables for path to theme.
  $variables['base_path'] = base_path();
  $variables['path_to_resbartik'] = drupal_get_path('theme', 'businesschool');

  // Add local.css stylesheet
  if (file_exists(drupal_get_path('theme', 'businesschool') . '/css/local.css')) {
    drupal_add_css(drupal_get_path('theme', 'businesschool') . '/css/local.css',
      array('group' => CSS_THEME, 'every_page' => TRUE));
  }

  // Add body classes if certain regions have content.
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])
  ) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])
  ) {
    $variables['classes_array'][] = 'footer-columns';
  }
	
	if (current_path() == 'node/115') {
		$variables['classes_array'][] = 'page-calendar';
	}
	
	if (request_uri() == '/travelling-programs') {
		$variables['classes_array'][] = 'page-travelling-programs';
    
    $metatag_arr = metatag_metatags_load('node', arg(1));    
    $variables['head_title'] = $metatag_arr['ru']['title']['value'];
    
	}
  
  if (request_uri() == '/programs') {  
  /*
    $view = views_get_view('cources_section');
    $view->set_display('block_2');   
    $title = $view->display['block_2']->display_options['metatags']['und']['title']['value'];    
    if (strstr($title, '[site:name]') ) {
      $title = str_replace('[site:name]', 'Бизнес-Эго',  $title );
    }   
    //$variables['head_title'] = $title;
  */  
    $variables['head_title'] = variable_get('programs_meta_title', 'Программы обучения | Бизнес-Эго');        
  }
  
  if (request_uri() == '/contacts') {
    $variables['head_title'] = variable_get('contacts_meta_title', 'Контакты | Бизнес-Эго');
  }

	drupal_add_css('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&subset=latin,cyrillic,cyrillic-ext', array('type' => 'external'));
 
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function businesschool_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}

/**
 * Override or insert variables into the page template.
 */
function businesschool_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name'] = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function businesschool_preprocess_maintenance_page(&$variables)
{
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'businesschool') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function businesschool_process_maintenance_page(&$variables)
{
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name'] = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the page template.
 */
function businesschool_preprocess_page(&$variables){
	
	if (!drupal_is_front_page() && isset($variables['node'])) {
		drupal_set_title($variables['node']->title);
	}
	
   // phpinfo();
	$path = drupal_get_path('theme', 'businesschool');
	
	if (drupal_is_front_page() || current_path() == 'reviews' || current_path() == 'programs' 
		|| request_uri() == '/calendar' || request_uri() == '/travelling-programs' || current_path() == 'node/175') {
		
		drupal_add_js($path . '/js/swiper.min.js');
		drupal_add_css($path . '/css/swiper.css');
		drupal_set_title('');
		
		drupal_add_js($path . '/js/jquery.expander.min.js');
		drupal_add_js($path . '/js/spoiler.js');
		
		if (current_path() == 'programs' || request_uri() == '/travelling-programs' || current_path() == 'node/175') {
			drupal_add_js($path . '/js/slider-programs.js');
		}
		
		if (request_uri() == '/calendar') {
			drupal_add_js($path . '/js/slider-calendar.js');
			drupal_add_js($path . '/js/mobile-spoiler-cources.js');
		}
		if (drupal_is_front_page() || current_path() == 'reviews') {
			drupal_add_js($path . '/js/slider.js');
		}
	}
	
	if (request_uri() == '/calendar') {
		
		/*$node_t = node_load(115);
		
		$title = $node_t->title;*/
		
		$obg = menu_get_object();
		
		$title = $obg->title;
		
		drupal_set_title($title);
		drupal_add_js($path . '/js/collapsible-menu-calendar.js');
	}
	
	if (request_uri() == '/programs') {
		drupal_set_title('Программы в Москве');
		//drupal_add_js($path . '/js/collapsible-menu-courses.js');
	}
	
	if (request_uri() == '/travelling-programs' || current_path() == 'node/175') {
		drupal_set_title('Выездные программы');
		//drupal_add_js($path . '/js/collapsible-menu-courses.js');
	}
	
	if (request_uri() == '/photos') {
		drupal_set_title('Фотогалерея');
	}
	
	if (request_uri() == '/reviews') {
		drupal_set_title('Отзывы');
	}
	
	if (request_uri() == '/blogs') {
		drupal_set_title('Журнал');
	}
	
	if (request_uri() == '/seminars') {
		drupal_set_title('Выездные программы');
	}
	
	if (request_uri() == '/about-us') {
		drupal_set_title('О нас');
	}
	
	if (request_uri() == '/teachers') {
		drupal_set_title('Преподаватели');
	}
	
  drupal_add_js($path . '/js/jquery.maskedinput.min.js');
  drupal_add_js($path . '/js/masked.js');
  drupal_add_js($path . '/js/sign-up.js');
	
	if (current_path() == 'node/132') {
    drupal_set_title('');     
    $variables['theme_hook_suggestions'][] = 'page__404';
  }
	
	if (request_uri() == '/contacts') {
    drupal_set_title('Контакты');
  }
	
	drupal_add_js('http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU', 'external');
	
	if ( !drupal_is_front_page()) {
		if (array_key_exists('node', $variables)) {
			$elements = array('em', 'sub', 'sup', 'br');
			// Reset title to allow safe HTML
			$variables['title'] = filter_xss($variables['node']->title, $elements);
			// Strip HTML from head title
			$variables['head_title'] = strip_tags($variables['node']->title);		
			drupal_set_title($variables['head_title']); 
		}
    
  }
  
  if (isset($variables['node']) && $variables['node']->type == 'cource' ) {
    
    drupal_add_js($path . '/js/spoiler_program.js');
    
  }
  
  
  //блок форма для оплаты курса
  $variables['payment_form'] = businesschool_get_rendered_block('busineschool_payment', 'busineschool_payment');
 
}

/**
 * Override or insert variables into the node template.
 */
function businesschool_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
	
	$node_type_suggestion_key = array_search('node__' . $variables['type'], $variables['theme_hook_suggestions']);
	$alter = array(
    'max_length' => 315,
    'word_boundary' => TRUE,
    'ellipsis' => TRUE,
    'html' => FALSE,
  );
	
	//echo '<pre>'; print_r($variables['node']); echo '</pre>'; die();
	
  if ($variables['type'] == 'cource' && $variables['view_mode']) {
		
		$variables['out_created'] = format_date($variables['created'], 'custom', 'd F Y'); 
		$variables['cource_teacher'] = '';		
		$variables['cource_shedule'] = '';		
		$variables['cource_price'] = '';		
		$variables['cource_place'] = '';		
		
		$variables['cource_date']  = isset($variables['field_cource_date'][0]) ? format_date(strtotime($variables['field_cource_date'][0]['value']), 'custom', 'd-m-Y') : '';
		
		if (isset($variables['field_cource_shedule']['und'])) {
			$variables['cource_shedule'] = $variables['field_cource_shedule']['und'][0]['value'];
		}
		
		if (isset($variables['field_cource_shedule']['und'])) {
			$variables['cource_price'] = $variables['field_cource_price']['und'][0]['value'];
		}
		
		if (isset($variables['field_cource_place']['und'])) {
			$variables['cource_place'] = $variables['field_cource_place']['und'][0]['value'];
		}
		   
		/*teacher*/
		if (isset($variables['field_cource_teacher'][0])) {
			
      	$variables['teacher_image_url'] = image_style_url('teachers_preview_160_160',
				$variables['field_cource_teacher'][0]['node']->field_teacher_photo_preview['und'][0]['uri']
			);
      
      
			$variables['teacher_name'] = l(
				$variables['field_cource_teacher'][0]['node']->title, 
				'node/' . $variables['field_cource_teacher'][0]['nid']
			) ;
			
			$variables['teacher_description'] = $variables['field_cource_teacher'][0]['node']->body['und'][0]['value'];
		}
		
		
		if (isset($variables['field_cource_description'][0])) {
			if ($variables['view_mode'] == 'full') {
				$variables['out_cource_description'] = $variables['field_cource_description'][0]['value'];
				
				$html_obj = new simple_html_dom();
				$html_obj->load($variables['out_cource_description']);				
				$sign_up_butt_ob = $html_obj->find('a.button', 0);
			
				if ($sign_up_butt_ob) {
				
					//busineschool_signup_include_modal();
					
					if ( isset($sign_up_butt_ob->attr['title']) ) {
						
						//echo $sign_up_butt_ob->attr['href']; die();						
						$innerText = $sign_up_butt_ob->attr['title']; 
						
						if ($sign_up_butt_ob->attr['href'] == '#') {						
							$href = '#signup-form-top';
						}
						$sign_up_butt_ob->outertext = 							
							'<span class="field-content sign-up course-' . $variables['nid'] . '">
													<a href="' . $href . '"  title="' . $innerText . '">' . $innerText .  '</a></span>';
						
									
						$variables['out_cource_description'] = $html_obj;
					}
					
				}
				//$html_obj->clear(); 
				
			}
		}	
		
		if (isset($variables['body'][0])) {
			$variables['out_body'] = $variables['body'][0]['value'];
			if ($variables['view_mode'] == 'teaser') {
				$variables['out_body'] = strip_tags(views_trim_text($alter, $variables['body'][0]['value']));
			}
			
		}
		
		if ($variables['view_mode'] == 'teaser') {
			$variables['theme_hook_suggestions'][] = 'node__cource__teaser';

      //echo $variables['field_cource_is_action'][0]['value']; 	
    }
    
    $cource_cost = $variables['field_cource_price'][0]['value'];
    
    //echo '<pre>'; print_r($variables['field_cource_is_action'][0]['value']); echo '</pre>'; die();
    
    // если акция, то
    if ( isset($variables['field_cource_is_action'][0]) && $variables['field_cource_is_action'][0]['value']) {
      $variables['is_action'] = true;
      
      //echo 'is_act';
      
      $cource_cost = $variables['field_cource_action_price'][0]['value'];
    }
    
    //echo $cource_cost; die();
    
    $variables['cource_cost'] = intval(str_replace(' ', '', $cource_cost));
    $variables['cource_title'] = $variables['title'];
    
    //echo intval(str_replace(' ', '', $cource_cost)); die();
			
  }
	
	if ($variables['view_mode'] == 'full') {
		// !!! todo для текстовых страниц
		
		if ( isset($variables['body'][0]) ) {
			  $variables['out_body'] = $variables['body'][0]['value'];
				$html_obj = new simple_html_dom();
		 
				$html_obj->load($variables['body'][0]['value']);				
				$sign_up_butt_ob = $html_obj->find('a.button', 0);
			
				if ($sign_up_butt_ob) {								
					if ( isset($sign_up_butt_ob->attr['title']) ) {						
						//echo $sign_up_butt_ob->attr['href']; die();						
						$innerText = $sign_up_butt_ob->attr['title']; 
											
							$href = '#signup-form-top';						
							$cource_href = $sign_up_butt_ob->attr['href'];
														
							global $base_url;							
							$cource_href = str_replace($base_url, '', $cource_href);							
							//echo $base_url . ' ' . $cource_href; die();
							
							if ($cource_href[0] == '/') {
								$cource_href = ltrim($cource_href, " \/");
							}

							$path = drupal_lookup_path("source", $cource_href);
							
							if ($path) {
								$nid = substr($path, 5, strlen($path) - 1);
							}
							
							if (isset($nid)) {
								$sign_up_butt_ob->outertext = 							
									'<span class="field-content sign-up course-' . $nid . '">
														<a class="course-bottom" href="' . $href . '"  title="' . $innerText . '">' . $innerText .  '</a></span>';
							
								$variables['out_body'] = $html_obj;
							}
									
						
					}
					
				}
				//$html_obj->clear(); 
				
		}
	}
	
	if ($variables['type'] == 'gallery') {
		$variables['images'] = array();
		if ($variables['field_gallery_image']) {
			foreach ($variables['field_gallery_image'] as $image_item) {
				
				$preview_image_uri = image_style_url('gallery_preview', $image_item['uri']);
				$full_image_uri = image_style_url('gallery_full_mobile', $image_item['uri']);
				
				$image_title = $image_item['title'] ? $image_item['title'] : $variables['title'];
				
				$variables['images'][] = array(
					'full_image_uri' => $full_image_uri,
					'preview_image_uri' => $preview_image_uri,
					'image_title' => $image_title,
				);
			}
		}
		
		$path = drupal_get_path('theme', 'businesschool');
		drupal_add_js($path . '/js/jquery.touchSwipe.min.js'); 
		drupal_add_js($path . '/js/swipe.js');
		drupal_add_js($path . '/js/colorbox-mobile.js');
		drupal_add_js($path . '/js/gallery.js');
		
	}
	
	if ($variables['type'] == 'cource') {
		$path = drupal_get_path('theme', 'businesschool');
		drupal_add_js($path . '/js/jquery.expander.min.js');
		drupal_add_js($path . '/js/spoiler_mobile.js'); 
	}
	
}

/**
 * Override or insert variables into the block template.
 */
function businesschool_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
	
	// добавим ctool ссылку для мобильного блока подачи заявки
	if ($variables['block']->css_class == 'mobile-sign-up') {
		busineschool_signup_include_modal();
		$apply_for_button = ctools_modal_text_button(strip_tags($variables['content']), 'signup/nojs/action', strip_tags($variables['content']), 'ctools-modal-busineschool-dialog-signup-style');
		$variables['content'] = '<p>' . $apply_for_button . '</p>';
	}
}

/**
 * Implements theme_menu_tree().
 */
function businesschool_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function businesschool_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}


/**
 * Overrides Theme function to output tablinks for classic Quicktabs style tabs.
 */
function businesschool_qt_quicktabs_tabset($vars) {
  $variables = array(
    'attributes' => array(
      'class' => 'quicktabs-tabs quicktabs-style-' . $vars['tabset']['#options']['style'],
    ),
    'items' => array(),
  );
  foreach (element_children($vars['tabset']['tablinks']) as $key) {
    $item = array();
    if (is_array($vars['tabset']['tablinks'][$key])) {
      $tab = $vars['tabset']['tablinks'][$key];
      $tab['#options']['html'] = TRUE; // Added this to override to allow HTML in titles.
      if ($key == $vars['tabset']['#options']['active']) {
        $item['class'] = array('active');
      }
      $item['data'] = drupal_render($tab);
      $variables['items'][] = $item;
    }
  }
  return theme('item_list', $variables);
}
