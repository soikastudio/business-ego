(function ($) {
  
  Drupal.behaviors.maskedForm = {
    attach: function (context, settings) {
      var phone_field =  $("#modal-content .form-item-phone input");
      var phone_field_modal =  $("#modal-content .field-name-field-application-phone input");
      
      var payment_form_phone =  $('#payment_form input[name="customerNumber"]');
      
       if (payment_form_phone) {
        $('input[name="customerNumber"]').mask("+7(999) 999-9999");
      }
      
      if (phone_field) {
        $("#modal-content .form-item-phone input").mask("+7(999) 999-9999");
      }
			
			if (phone_field_modal) {
        $("#modal-content .field-name-field-application-phone input").mask("+7(999) 999-9999");
      }
				
    }
  }
    
})(jQuery);