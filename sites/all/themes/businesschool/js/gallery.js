(function ($) {
	
  $(document).ready(function(){
			
		var m_match = window.matchMedia("(max-width: 768px)");  
		if (m_match.matches) {
			document.getElementById('links').onclick = function (event) {
				event = event || window.event;
				var target = event.target || event.srcElement,
						link = target.src ? target.parentNode : target,
						options = {index: link, event: event},
						links = this.getElementsByTagName('a');
				blueimp.Gallery(links, options);
			}
		};
				 		 
  });
  
})(jQuery);