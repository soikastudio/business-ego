(function ($) {

	Drupal.behaviors.slidersCalendar = {
    attach: function (context, settings) {
			
			var swiper_calendar_mobile = undefined;
			
			function initSwiper() {			
				var screenWidth = $(window).width();
				if(screenWidth <= 768 && swiper_calendar_mobile == undefined) {
						console.log('init calendar sliders');
						 
						//for (var i = 0; i <= 2; i++) {
							//slider_tabs_num = i + 1;						
							//swiper_sliders[i] = new Swiper('#quicktabs-busineschool_common_calendar #tabs-' + slider_tabs_num + ' .view-content', {					
						//swiper_calendar_mobile = new Swiper('#quicktabs-busineschool_common_calendar #tabs-1 .view-content', {					
						swiper_calendar_mobile = new Swiper('#block-busineschool-common-busineschool-common-calendar .view-content', {					
							pagination: '.cources-pagination',
							nextButton: '.swiper-cources-next',
							prevButton: '.swiper-cources-prev',					
							centeredSlides: true,
							slidesPerView: 1,
							paginationClickable: true,
							loop: true,
							spaceBetween: 10,					
							centeredSlides: true,
						});
														
						
						$('.item-list-wrapper').css('padding', '3% 7.5% 0');
				}
				else if (screenWidth > 768 && swiper_calendar_mobile != undefined) {
					
					swiper_calendar_mobile = undefined;
					
					jQuery('.swiper-wrapper').removeAttr('style');
					jQuery('.swiper-slide').removeAttr('style');

					$('.item-list-wrapper').css('padding', '0');
						
					  //$('.block .quicktabs-tabs').css('display', 'block');

				}
				
							
			}

		//Swiper plugin initialization
			initSwiper();

			//Swiper plugin initialization on window resize
			$(window).on('resize', function(){
					initSwiper();        
			});
			
			//$('#quicktabs-busineschool_common_calendar').tabs();
		
							
    }
		
  }
	  
})(jQuery);