(function($) {
		
	$.colorbox.settings.onComplete= function() {
   // var colorboxHeight = $('#cboxLoadedContent').height();
		
		$("#colorbox").swipe( {
			
			//Generic swipe handler for all directions
			swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
				console.log(direction);
				
				if (direction == 'right') {
					$('#cboxPrevious').trigger('click');
				}

				if (direction == 'left') {
					$('#cboxNext').trigger('click');
				}
			},
			//Default is 75px, set to 0 for demo so any distance triggers swipe
			 threshold:0
		});
  };
  
  /**
   * Adds toggle link.
   * Toggles menu on small resolutions.
   * Restores menu on window width increasing.
   */
  Drupal.behaviors.busineschoolColorbox = {
    attach: function (context, settings) {
			
		
				/*
				$("#colorbox").on('swipe', {
					
					//Generic swipe handler for all directions
					swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
						console.log(direction);
						
						if (direction == 'right') {
							$('#cboxPrevious').trigger('click');
						}

						if (direction == 'left') {
							$('#cboxNext').trigger('click');
						}
					},
					//Default is 75px, set to 0 for demo so any distance triggers swipe
					 threshold:0
				});
				*/

        /*				
				$("#colorbox").swipe( {
					
					//Generic swipe handler for all directions
					swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
						console.log(direction);
						
						if (direction == 'right') {
							$('#cboxPrevious').trigger('click');
						}

						if (direction == 'left') {
							$('#cboxNext').trigger('click');
						}
					},
					//Default is 75px, set to 0 for demo so any distance triggers swipe
					 threshold:0
				});
				
	
			})
     */
    }
  }
		
		$(document).ready(function () {
			var size_items = $(".gallery-mobile .field-item").size();
			var x = 3;
			$('.gallery-mobile .field-item:lt('+ x +')').show();
			$('.pager-load-more a').click(function () {
					x = (x + 6 <= size_items) ? x + 6 : size_items;
					$('.gallery-mobile .field-item:lt('+x+')').show();
					
					//console.log(x);
					
					if (x == size_items) {
						$(this).parent().parent().hide();
					}
					
					return false;
			});
			
			/*
			$('#showLess').click(function () {
					x=(x-5<0) ? 3 : x-5;
					$('#myList li').not(':lt('+x+')').hide();
			});
			*/
		});
    
    
    
  
})(jQuery);



