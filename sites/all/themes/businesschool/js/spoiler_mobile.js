(function ($) {
	
  $(document).ready(function(){
		
		var m_match = window.matchMedia("(max-width: 480px)");  
		if (m_match.matches) {
					
		  // курс
			$('.field-name-field-cource-description').expander({
				slicePoint: 400,
				expandText: 'Читать полностью',
				//expandText: '',
				userCollapseText: 'Свернуть',
				preserveWords: true,
				widow: 2,
				
				expandSpeed: 500,
				collapseEffect: 'slideUp',
				collapseSpeed: 400,
				
				expandPrefix: '',
				
			});
			
		}
		
	
  });
  
})(jQuery);