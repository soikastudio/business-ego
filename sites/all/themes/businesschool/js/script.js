(function ($) {
	
	Drupal.behaviors.customSelectBox = {
    attach: function (context, settings) {
						
			if (context == '#modalContent') {
				
				$("#modal-content select.custom").each(function() {
		
					var sb = new SelectBox({
						selectbox: $(this),
						height: 100,
						width: '100%'
						
					});
				});
						
			}
			
			if ($('.customSelect').length > 0) {
				//console.log($('.customSelect'));
			}
      
      
      $('.form-item-terms-of-use input').click(function() {
        
        if ($(this).is(':checked')) {
          console.log();
          $('.form-submit').removeAttr( "disabled" )
        }
        else {
          $('.form-submit').attr("disabled", "disabled");
        }
      
      })
			
    }
    
   
				
  }
	
	$(window).scroll(function(){ 
	 //background: rgba(249,239,217,0.8);
	 
	  var screenWidth = $(window).width();
	 
	  if(screenWidth > 768) {
			var top =  $(window).scrollTop(); 
			//console.log(top);
			$('#header-top-wrapper').css('background', 'url(/sites/all/themes/businesschool/images/fixed-menu-bg.png) rgb(249,239,217) right no-repeat') ;
			if (top == 0) {
				$('#header-top-wrapper').css('background', 'none');
			}
		}
	 
	})
	
	
  $(document).ready(function(){
	
		$("#block-busineschool-signup-busineschool-signup-form select.custom").each(function() {
	
			var sb = new SelectBox({
				selectbox: $(this),
				height: 100,
				width: '96%'
			});
			
			$('.customSelect').click(function(){
				var isSelectOpenned = $(this).hasClass('select-open');
				if (isSelectOpenned) {
					$('.form-actions.form-wrapper').hide();
				}
				else {
					$('.form-actions.form-wrapper').show();
				}
			})
			
		});
		
		$('#block-views-cources-block-1').click(function() {			
			window.location.href = '/programs';			
		})
		
		$('#block-views-cources-block-1 .views-field-nothing').click( function() {			
			window.location.href = '/programs';			
		})
		
		$('#block-views-cources-block-1 h2').click( function() {			
			window.location.href = '/programs';			
		})
		
		 
		$('#block-views-seminars-block').click(function() {			
			window.location.href = '/travelling-programs';			
		})
		
		$('#block-views-seminars-block .views-field-nothing-1').click( function() {			
			window.location.href = '/travelling-programs';			
		})
		
		$('#block-views-seminars-block h2').click( function() {			
			window.location.href = '/travelling-programs';			
		})
		//.views-field views-field-nothing
    
    /*
    // на странице продукта
    $('.node-cource-full-content .popup__link').click(function(){
      
      $('#popup__overlay').addClass('popup_show').show();
      $('#payment_form').addClass('popup_show').show();
      
      // заполняем данные о программе
      var cource_title = $('#page-title').text();
      //console.log(cource_title);
      
      $('.form-item-orderDetails textarea').html($.trim(cource_title));
      
      var price = $(this).parent().prev().prev().find('.cource_cost').text(); //$('.cource_cost').text();
      
      $('input[name="sum"]').val(price + '.00');
      
    })
    */
    
    // кнопка на странице календаря
    $('.popup__link').click(function(){
      
      $('#popup__overlay').addClass('popup_show').show();
      $('#payment_form').addClass('popup_show').show();
      
      // заполняем данные о программе   
      var cource_title = $(this).attr('data-title');    
      $('.form-item-orderDetails textarea').html('Оплата участия в программе "' + $.trim(cource_title) + '"');
           
      var price = $(this).attr('data-price'); //$('.cource_cost').text();
      
      //console.log($(this).attr('data-price'));      
      $('input[name="sum"]').val(price + '.00');
      
      if ($(this).hasClass('reserve-programm')) {
        $('#payment_form h3').text('Бронирование');
        $('.form-item-orderDetails textarea').html('Бронирование места для участия в программе "' + $.trim(cource_title) + '"');
      }
      
    })
    
   
    
    // кнопка закрытия попап
    $('button.popup__close').click(function(){
      
      $('#popup__overlay').removeClass('popup_show').hide();
      $('#payment_form').removeClass('popup_show').hide();
      
    })
		
  });
  
})(jQuery);