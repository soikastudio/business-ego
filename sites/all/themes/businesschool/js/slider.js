(function ($) {
	
	var swiper_cources;
	var swiper_blogs;
		
	Drupal.behaviors.slidersBusiness = {
    attach: function (context, settings) {
		
			
			 var swiper = new Swiper('.view-id-slider.view-display-id-block .view-content', {
					
					pagination: '#banner-pagination',
					nextButton: '#swiper-banner-next',
					prevButton: '#swiper-banner-prev',
					
					centeredSlides: true,
					slidesPerView: 1,
					paginationClickable: true,
					loop: true,
					spaceBetween: 20,
          speed: 1500,
          autoplay: 3000
					
					/*centeredSlides: true,*/
			});
			
			var swiper_reviews = new Swiper('.view-id-reviews.view-display-id-block  .view-content', {
					
					pagination: '#reviews-pagination',
					nextButton: '#swiper-reviews-next',
					prevButton: '#swiper-reviews-prev',
					
					centeredSlides: true,
					slidesPerView: 1,
					paginationClickable: true,
					loop: true,
					spaceBetween: 130
					
					/*centeredSlides: true,*/
			});
			
			// mobile sliders
			var m_match = window.matchMedia("(max-width: 768px)");  
			if (m_match.matches) {
				
				$('#cources-pagination, #swiper-cources-next, #swiper-cources-prev, #blogs-pagination, #swiper-blogs-next,#swiper-blogs-prev').show();	
				
				
				  swiper_cources = new Swiper('.view-id-cources.view-display-id-block .view-content', {
					
					pagination: '#cources-pagination',
					nextButton: '#swiper-cources-next',
					prevButton: '#swiper-cources-prev',
					
					centeredSlides: true,
					slidesPerView: 1,
					paginationClickable: true,
					loop: true,
					spaceBetween: 20
					
				});
				
				swiper_blogs = new Swiper('.view-id-blogs.view-display-id-block .view-content', {
					
					pagination: '#blogs-pagination',
					nextButton: '#swiper-blogs-next',
					prevButton: '#swiper-blogs-prev',
					
					centeredSlides: true,
					slidesPerView: 1,
					paginationClickable: true,
					loop: true,
					spaceBetween: 20
					
				});
				
				var swiper_reviews_mobile = new Swiper('.view-id-reviews.view-display-id-page .view-content', {
					
					pagination: '#reviews-pagination',
					nextButton: '#swiper-reviews-next',
					prevButton: '#swiper-reviews-prev',
					
					centeredSlides: true,
					slidesPerView: 1,
					paginationClickable: true,
					loop: true,
					spaceBetween: 10,
					
					centeredSlides: true
				});
				
				
			}
     
    }
  }
	
		
  $(document).ready(function(){
   
  });
	
	//activate disactivat sliders
	$(window).resize( function() {
		var w_width = $(window).width();		
		if (w_width == 768) {
										
			$('#cources-pagination, #swiper-cources-next, #swiper-cources-prev, #blogs-pagination, #swiper-blogs-next,#swiper-blogs-prev').show();	
							
			swiper_cources = new Swiper('.view-id-cources.view-display-id-block .view-content', {
				
				pagination: '#cources-pagination',
				nextButton: '#swiper-cources-next',
				prevButton: '#swiper-cources-prev',
				
				centeredSlides: true,
				slidesPerView: 1,
				paginationClickable: true,
				loop: true,
				spaceBetween: 20
				
			});
			
			swiper_blogs = new Swiper('.view-id-blogs.view-display-id-block .view-content', {
				
				pagination: '#blogs-pagination',
				nextButton: '#swiper-blogs-next',
				prevButton: '#swiper-blogs-prev',
				
				centeredSlides: true,
				slidesPerView: 1,
				paginationClickable: true,
				loop: true,
				spaceBetween: 20
				
			});				
				
		}
					
		if (w_width > 768 && w_width <= 1024) {	
      if (swiper_cources)		{
				console.log('destr');
				swiper_cources.destroy(true, true);
				swiper_blogs.destroy(true, true);
			}
			
			$('#cources-pagination, #swiper-cources-next, #swiper-cources-prev, #blogs-pagination, #swiper-blogs-next,#swiper-blogs-prev').hide();
		}
		
		
	});
  
})(jQuery);