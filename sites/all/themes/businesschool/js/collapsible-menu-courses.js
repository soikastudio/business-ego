/**
 * Behaviors for collapsible menu.
 */
(function($) {

  /**
   * Adds toggle link.
   * Toggles menu on small resolutions.
   * Restores menu on window width increasing.
   */
  Drupal.behaviors.responsiveBartikCollapsibleMenuCources = {
    attach: function (context, settings) {

      //console.log('class');
     //var page_title = $('#page-title').text();
      // We can keep menu collapsed up to width maxWidth.
      var maxWidth = 768;
			
			// mobile sliders
			
			function initMenu() {
				var m_match = window.matchMedia("(max-width: 768px)");  
				if (m_match.matches) {
					//console.log('in');
					// Do nothing if menu is empty.
					if ($('#quicktabs-cources_programm ul.quicktabs-tabs.quicktabs-style-nostyle a').length == 0) {
						return;
					}

					// Append toggle link to the main menu if not already exists.
					if ($('a#menu-toggle-cources').length == 0) {
						
						var first_tab_text = $('.quicktabs-tabs.quicktabs-style-nostyle li.first a').text();
						$('#quicktabs-cources_programm .item-list').prepend('<a href="#" id="menu-toggle-cources">' + first_tab_text + '</a>');
						
						$('#quicktabs-cources_programm .item-list').wrap( "<div class='item-list-wrapper'></div>" );
					}
					else {
						return;
					}
					
					$('#quicktabs-cources_programm .item-list ul li a').click(function(){
						//$('.item-list-wrapper a#menu-toggle-cources').trigger('click').text($(this).text());
						$('.item-list-wrapper a#menu-toggle-cources').text($(this).text());
						$('a#menu-toggle-cources').addClass('collapsed').removeClass('expanded').css('border-bottom', 'none');
						$('#quicktabs-cources_programm ul.quicktabs-tabs.quicktabs-style-nostyle').hide();
						$('.item-list-wrapper').css('padding', '3% 7.5% 0');
						
						if ($(this).attr('id') == 'quicktabs-tab-cources_programm-0') {
							console.log('0');
							$('#quicktabs-tab-cources_programm-1').parent('li').css('border-bottom', '2px solid #e3ce82')
						}
						
					})
												 
					// Collapse/expand menu by click on link.
					$('.item-list-wrapper a#menu-toggle-cources').click(function() {
						
						var display = $('div#block-quicktabs-cources-programm .item-list .quicktabs-tabs').css('display');
						if (display == 'none') {
							$(this).css('border-bottom', '2px solid #e3ce82');						
							// убираем border у последнего не активного элемента
							var li_items = $('div#block-quicktabs-cources-programm .item-list ul.quicktabs-tabs li:not(.active)');													
							$(li_items[li_items.length - 1]).css('border-bottom', 'none');							
													
							$('.item-list-wrapper').css('padding', '3% 7.5% 45px 7.5%');						
						}
						
						else {
							$(this).css('border-bottom', 'none');
							
							// возвращаем border у последнего не активного элемента
							var li_items = $('div#block-quicktabs-cources-programm .item-list ul.quicktabs-tabs li:not(.active)');
							$(li_items[li_items.length - 1]).css('border-bottom', '2px solid #e3ce82');
							
							$('.item-list-wrapper').css('padding', '3% 7.5% 0');
						}
						
						
						$('#quicktabs-cources_programm ul.quicktabs-tabs.quicktabs-style-nostyle').slideToggle('fast', function(){						
							var display = $(this).css('display'); 										
							if (display == 'none') {
								$('a#menu-toggle-cources').addClass('collapsed').removeClass('expanded');
								$('#quicktabs-cources_programm ul.quicktabs-tabs.quicktabs-style-nostyle').hide();
							}
							else {
								//$('#block-menu-menu-our-classes').css('border-color', '#fcb417');
								$('a#menu-toggle-cources').addClass('expanded').removeClass('collapsed');
								$('#quicktabs-cources_programm ul.quicktabs-tabs.quicktabs-style-nostyle').show();
							}
							
							return false;
						});
												
						return false;
					});

				}
				
				
				else {
					var parent_item_list = $('#quicktabs-cources_programm .item-list').parent();					
					if ($(parent_item_list).hasClass("item-list-wrapper")) {
						$('#quicktabs-cources_programm .item-list').unwrap();
						$('#menu-toggle-cources').remove();
					}
				}
				
			}

			initMenu();
			
			$(window).resize(function(){
				
				initMenu();
				
				// Restore visibility settings of menu on increasing of windows width over 768px.
				// Media query works with width up to 768px. But I guess we should take into account some padding.
				var w = $(window).width();
				// Remove all styles if window size more than maxWidth and menu is hidden.
				if(w > maxWidth && $('#quicktabs-cources_programm ul.quicktabs-tabs.quicktabs-style-nostyle').is(':hidden')) {
					$('#quicktabs-cources_programm ul.quicktabs-tabs.quicktabs-style-nostyle').removeAttr('style');
				}
			});
			
		
    }
  }
})(jQuery);
