/**
 * Behaviors for collapsible menu.
 */
(function($) {
	
	function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}

  /**
   * Adds toggle link.
   * Toggles menu on small resolutions.
   * Restores menu on window width increasing.
   */
  Drupal.behaviors.CollapsibleMenuAbout = {
    attach: function (context, settings) {
			
      //console.log(getAbsolutePath());
			
			var path_arr = [ '/contacts', "/photos", "/reviews", "/about-us", "/teachers" ];
			
      //console.log(window.location.pathname);
			
			if (jQuery.inArray(window.location.pathname, path_arr) != -1) {
				var page_title = $('#page-title').text();
			}
			else {
				var page_title = "Фотогалерея";
			}
			
      
      // We can keep menu collapsed up to width maxWidth.
      var maxWidth = 768;

      // Do nothing if menu is empty.
      if ($('#block-menu-menu-about-us .menu a').length == 0) {
        return;
      }

      // Append toggle link to the main menu if not already exists.
      if ($('a#menu-toggle-about').length == 0) {
      	$('#block-menu-menu-about-us .content').prepend('<a href="#" id="menu-toggle-about">' + page_title + '</a>');
        
        $('#block-menu-menu-about-us h2').hide();       
         //add anchors to links 
 
       
      }
      else {
      	return;
      }
      
      // Collapse/expand menu by click on link.
      $('a#menu-toggle-about').click(function() {
				
				
				var display = $('#block-menu-menu-about-us .menu').css('display');
				
				if (display == 'none') {
					$(this).css('border-bottom', '2px solid #e3ce82').css('margin-bottom', '10px').css('padding-bottom', '19px')
					.css('background-position', '100% -34px');
					
					$('#block-menu-menu-about-us').css('padding', '7.5% 7.5% 45px 7.5%');
				
				  	// убираем border у последнего не активного элемента
					var li_items = $('#block-menu-menu-about-us .menu li:not(.active-trail)');
					$(li_items[li_items.length - 1]).css('border-bottom', 'none');
				}
				
				else {
					$(this).css('border-bottom', 'none').css('margin-bottom', '0').css('padding-bottom', '10px').css('background-position', '100% 10px');
					$('#block-menu-menu-about-us').css('padding', '7.5% 7.5% 0');
					
						// возвращаем border у последнего не активного элемента
					var li_items = $('#block-menu-menu-about-us .menu li:not(.active-trail)');
					$(li_items[li_items.length - 1]).css('border-bottom', '2px solid #e3ce82');
				}
		
        $('#block-menu-menu-about-us .menu').slideToggle('fast', function(){
          
          var display = $(this).css('display');
          
          if (display == 'none') {
            //$('#block-menu-menu-about-us').css('border-color', '#b0b0b0'); 
            $('a#menu-toggle-about').addClass('collapsed');
            $('a#menu-toggle-about').removeClass('expanded');
						
						/*header*/
						$('.not-front #header').css('position', 'static');
						
						
						/**/
          }
          else {
            //$('#block-menu-menu-about-us').css('border-color', '#fcb417');
            $('a#menu-toggle-about').addClass('expanded');
            $('a#menu-toggle-about').removeClass('collapsed');
						
						$('.not-front #header').css('z-index', '2').css('position', 'relative');
						
						//$('.not-front #main')
						
          }
          
          //console.log(display);
        });
        
       
        return false;
      });

      // Restore visibility settings of menu on increasing of windows width over 445px.
      // Media query works with width up to 460px. But I guess we should take into account some padding.
      $(window).resize(function(){
        var w = $(window).width();
        // Remove all styles if window size more than maxWidth and menu is hidden.
        if(w > maxWidth && $('#block-menu-menu-about-us .menu').is(':hidden')) {
          $('#block-menu-menu-about-us .menu').removeAttr('style');
          $('#block-menu-menu-about-us h2').show(); 
        }
      });
    }
  }
})(jQuery);
