(function ($) {
  
  var cource_nid = '';
  
  Drupal.behaviors.singUpForm = {
    attach: function (context, settings) {
      
      var modalWebform = $('.node-application_form-form');     
      if (modalWebform) {
        $('#modal-content .field-name-field-application-cource select').val(cource_nid);			 
      }
            
      $('.sign-up a.ctools-use-modal').click(function(){
				
				var span_id = $(this).parent();				
				var class_val = span_id.attr('class');				
				var nid = class_val.substring(29, class_val.length);
				
				cource_nid = nid;
				
        return false;        
      })

			$('.sign-up a.course-bottom').click(function(){
				
				var span_id = $(this).parent();				
				var class_val = span_id.attr('class');				
				var nid = class_val.substring(29, class_val.length);
				
				var cource_nid = nid;				
				if (cource_nid) {
					
					//console.log(cource_nid);					
					jQuery(".field-name-field-application-cource select option[value='"+cource_nid +"']").attr('selected', 'selected');
					$('.field-name-field-application-cource select').keyup();					
					$(".field-name-field-application-cource select option[value='"+cource_nid +"']").click();
					$('.field-name-field-application-cource select').val(cource_nid);
				
				}
				
        //return false;        
      })
      
     
    }
  }
  
  
  $(document).ready(function(){   
		//console.log('sign-up');
  });
  
})(jQuery);