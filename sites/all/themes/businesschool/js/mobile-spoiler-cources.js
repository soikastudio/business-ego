(function ($) {
	
  $(document).ready(function(){
		
		var m_match = window.matchMedia("(max-width: 480px)");  
		if (m_match.matches) {
					
		  // курс
			$('.view-cources .body').expander({
				slicePoint: 120,
				expandText: 'Подробное описание',
				//expandText: '',
				userCollapseText: 'Свернуть',
				preserveWords: true,
				widow: 2,
				
				expandSpeed: 500,
				collapseEffect: 'slideUp',
				collapseSpeed: 400,
				
				expandPrefix: '',
				
			});
			
		}
		
	
  });
  
})(jQuery);