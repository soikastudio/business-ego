(function ($) {
	
  $(document).ready(function(){
		
		var m_match = window.matchMedia("(max-width: 480px)");  
		if (m_match.matches) {
		
		  // блок отзывы
			$('.reviews-body.mobile').expander({
				slicePoint: 200,
				expandText: 'Читать полностью',
				//expandText: '',
				userCollapseText: 'Свернуть',
				preserveWords: true,
				widow: 2,
				
				expandSpeed: 500,
				collapseEffect: 'slideUp',
				collapseSpeed: 400,
				
				expandPrefix: ''
				
			});
						
			$('.view-id-reviews.view-display-id-page .views-field-body').expander({
				slicePoint: 200,
				expandText: 'Читать полностью',
				//expandText: '',
				userCollapseText: 'Свернуть',
				preserveWords: true,
				widow: 2,
				
				expandSpeed: 500,
				collapseEffect: 'slideUp',
				collapseSpeed: 400,
				
				expandPrefix: ''
				
			});
						
		}
		
		var m_match_sm = window.matchMedia("(max-width: 320px)");  
		if (m_match_sm.matches) {
		// блок блог
			$('.view-id-blogs.view-display-id-block .views-field-body .field-content, .view-id-cources.view-display-id-block .views-field-body .field-content').expander({
				slicePoint: 130,
				expandText: '',
				userCollapseText: '',
				preserveWords: true,
				widow: 2,
				
				expandSpeed: 500,
				collapseEffect: 'slideUp',
				collapseSpeed: 400
				
				//expandPrefix: '...',
				
			});
		}
			 
		 
  });
  
})(jQuery);