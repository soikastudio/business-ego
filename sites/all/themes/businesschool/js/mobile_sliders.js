(function ($) {
	
  $(document).ready(function(){
		
		var m_match = window.matchMedia("(max-width: 480px)");  
		if (m_match.matches) {
		
			$('.reviews-body.mobile').expander({
				slicePoint: 200,
				expandText: 'Читать полностью',
				//expandText: '',
				userCollapseText: 'Свернуть',
				preserveWords: true,
				widow: 2,
				
				expandSpeed: 500,
				collapseEffect: 'slideUp',
				collapseSpeed: 400,
				
				expandPrefix: '',
				
			});
			
			
			$('.view-id-reviews.view-display-id-page .views-field-body').expander({
				slicePoint: 200,
				expandText: 'Читать полностью',
				//expandText: '',
				userCollapseText: 'Свернуть',
				preserveWords: true,
				widow: 2,
				
				expandSpeed: 500,
				collapseEffect: 'slideUp',
				collapseSpeed: 400,
				
				expandPrefix: '',
				
			});
						
		}
			 
		 
  });
  
})(jQuery);