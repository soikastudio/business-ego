(function ($) {
	
	//var swiper_programs_mobile;
		
	Drupal.behaviors.slidersBusiness = {
    attach: function (context, settings) {
						
			var swiper_programs_mobile = undefined;
			
			//var swiper_programs_mobile1 = undefined;
			//var swiper_programs_mobile2 = undefined;
			//var swiper_programs_mobile3 = undefined;
			
			//var swiper_sliders = [swiper_programs_mobile1, swiper_programs_mobile2, swiper_programs_mobile3];
								
			function initSwiper() {
				 				
				var screenWidth = $(window).width();
				if(screenWidth <= 768 && swiper_programs_mobile == undefined) {
						console.log('init slider programs');
						 
						//for (var i = 0; i <= 2; i++) {
							//slider_tabs_num = i + 1;						
							//swiper_sliders[i] = new Swiper('#quicktabs-busineschool_common_calendar #tabs-' + slider_tabs_num + ' .view-content', {					
							/*
							
							*/
							
						//swiper_programs_mobile = new Swiper('.quicktabs-tabpage:not(.quicktabs-hide) .view-cources-section.view-id-cources_section .view-content', {						
						swiper_calendar_mobile = new Swiper('#block-quicktabs-cources-programm .view-content', {							
							pagination: '.cources-pagination',
							nextButton: '.swiper-cources-next',
							prevButton: '.swiper-cources-prev',					
							centeredSlides: true,
							slidesPerView: 1,
							paginationClickable: true,
							loop: true,
							spaceBetween: 10,					
							centeredSlides: true,						
						});
						
						$('.item-list-wrapper').css('padding',  '3% 7.5% 0');
						
							
							//swiper_programs_mobile = 'init';
							
						//}
				} else if (screenWidth > 768 && swiper_programs_mobile != undefined) {
					
						//swiper_programs_mobile.destroy();
						swiper_programs_mobile = undefined;
						
						jQuery('.swiper-wrapper').removeAttr('style');
						jQuery('.swiper-slide').removeAttr('style');
              
            $('.item-list-wrapper').css('padding',  '0');							
				}
				
				//$('#quicktabs-busineschool_common_calendar').tabs();			
			}

		//Swiper plugin initialization
			initSwiper();

			//Swiper plugin initialization on window resize
			$(window).on('resize', function(){
					initSwiper();        
			});
			
			//$('#quicktabs-busineschool_common_calendar').tabs();
			
		}
		
  }
	
	
	$(document).ajaxComplete(function() {		
		//swiper_programs_mobile.update();			
	});
	
	  
})(jQuery);