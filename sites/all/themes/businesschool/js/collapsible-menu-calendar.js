/**
 * Behaviors for collapsible menu.
 */
(function($) {

  /**
   * Adds toggle link.
   * Toggles menu on small resolutions.
   * Restores menu on window width increasing.
   */
  Drupal.behaviors.responsiveBartikCollapsibleMenuCalendar = {
    attach: function (context, settings) {

      // We can keep menu collapsed up to width maxWidth.
      var maxWidth = 768;
			
			// mobile sliders			
			function initMenu() {
				var m_match = window.matchMedia("(max-width: 768px)");  
				if (m_match.matches) {

					// Do nothing if menu is empty.
					if ($('#quicktabs-calendar_blocks ul.quicktabs-tabs.quicktabs-style-nostyle a').length == 0) {
						return;
					}

					// Append toggle link to the main menu if not already exists.
					if ($('a#menu-toggle-calendar').length == 0) {
						var first_tab_text = $('.quicktabs-tabs.quicktabs-style-nostyle li.first a .month-name').text();
						$('#quicktabs-calendar_blocks .item-list').prepend('<a href="#" id="menu-toggle-calendar">' + first_tab_text + '</a>');
						
						$('#quicktabs-calendar_blocks .item-list').wrap( "<div class='item-list-wrapper'></div>" );
					}
					else {
						return;
					}
					
					$('#quicktabs-calendar_blocks .item-list ul li a').click(function(){
						
						var month_name = $(this).children('.month-name');
						$('.item-list-wrapper #menu-toggle-calendar').trigger('click').text($(month_name).text());					
					  $('a#menu-toggle-calendar').addClass('collapsed').removeClass('expanded').css('border-bottom', 'none');
						$('#quicktabs-calendar_blocks ul.quicktabs-tabs.quicktabs-style-nostyle').hide();
						$('.item-list-wrapper').css('padding', '3% 7.5% 0');						
					
					})
											 
					// Collapse/expand menu by click on link.
					$('.item-list-wrapper a#menu-toggle-calendar').click(function() {
						
						var display = $('div#quicktabs-calendar_blocks .item-list .quicktabs-tabs').css('display');
						if (display == 'none') {
							$(this).css('border-bottom', '2px solid #e3ce82');
							
							// убираем border у последнего не активного элемента
							//quicktabs-calendar_blocks2
							var li_items = $('div#quicktabs-calendar_blocks .item-list ul.quicktabs-tabs li:not(.active)');
							$(li_items[li_items.length - 1]).css('border-bottom', 'none');
							
							$('.item-list-wrapper').css('padding', '3% 7.5% 45px 7.5%');
						}
						
						else {
							$(this).css('border-bottom', 'none');							
							// возвращаем border у последнего не активного элемента
							var li_items = $('div#quicktabs-calendar_blocks .item-list ul.quicktabs-tabs li:not(.active)');
							$(li_items[li_items.length - 1]).css('border-bottom', '2px solid #e3ce82');
							
							$('.item-list-wrapper').css('padding', '3% 7.5% 0');
						
						}
						
						
						$('#quicktabs-calendar_blocks ul.quicktabs-tabs.quicktabs-style-nostyle').slideToggle('fast', function(){
							
							var display = $(this).css('display'); 						
							if (display == 'none') {
								$('a#menu-toggle-calendar').addClass('collapsed').removeClass('expanded');
								$('#quicktabs-calendar_blocks ul.quicktabs-tabs.quicktabs-style-nostyle').hide();
							}
							else {
								$('a#menu-toggle-calendar').addClass('expanded').removeClass('collapsed');
								$('#quicktabs-calendar_blocks ul.quicktabs-tabs.quicktabs-style-nostyle').show();
							}
							
						});
												
						return false;
					});
				}
				
				else {
					
					var parent_item_list = $('#quicktabs-calendar_blocks .item-list').parent();					
					if ($(parent_item_list).hasClass("item-list-wrapper")) {
						console.log('has');
						$('#quicktabs-calendar_blocks .item-list').unwrap();
						$('#menu-toggle-calendar').remove();
					}
					
				}
			}
			
			initMenu();
			
			
			$(window).resize(function(){
				
				initMenu();
				
				// Restore visibility settings of menu on increasing of windows width over 445px.
			// Media query works with width up to 460px. But I guess we should take into account some padding.
				var w = $(window).width();
				// Remove all styles if window size more than maxWidth and menu is hidden.
				if(w > maxWidth && $('#quicktabs-calendar_blocks ul.quicktabs-tabs.quicktabs-style-nostyle').is(':hidden')) {
					$('#quicktabs-calendar_blocks ul.quicktabs-tabs.quicktabs-style-nostyle').removeAttr('style');
				}
			});
		
    }
  }
})(jQuery);
