<div class="contextual-links-region">
	<?php print busineschool_common_make_contextual_links('', $node->nid); ?>
	<div class="node-cource-full">

			<div class="node-cource-full-header">
				<?php if ($out_body):?>
						<div class="body"><?php print $out_body; ?></div>
					<?php endif; ?>
			</div>
				
			<div class="node-cource-full-content">
				<div class="col-1">
					
					<?php if (isset($content['field_cource_description'])): ?>
						<?php //print render($content['field_cource_description']); ?>
						<div class="field field-name-field-cource-description field-type-text-long field-label-above">
							<div class="field-label">Описание программы:&nbsp;</div>
						</div>
						<?php print $out_cource_description; ?>
					<?php endif; ?>
					
					<?php if (isset($content['field_cource_goal'])): ?>
						<?php print render($content['field_cource_goal']); ?>
					<?php endif; ?>
							
					<!--<div class="sign-up"><a href="#">Записаться на курс</a></div> -->
					
					<?php if (isset($content['field_cource_image'])): ?>
						<?php print render($content['field_cource_image']); ?>
					<?php endif; ?>
				</div>
        
        <div class="col-1 mobile">
					
					<?php if (isset($content['field_cource_description'])): ?>
						<?php print render($content['field_cource_description']); ?>
					<?php endif; ?>
					
					<?php if (isset($content['field_cource_goal'])): ?>
						<?php print render($content['field_cource_goal']); ?>
					<?php endif; ?>
									
					<?php if (isset($content['field_cource_image'])): ?>
						<?php print render($content['field_cource_image']); ?>
					<?php endif; ?>
				</div>

				<div class="col-2">

				 <?php if (isset($content['field_cource_teacher'])): ?>
				 <!--<div class="teacher-title">Автор и ведущий программы</div> -->
					<div class="teacher">
						<?php //print render($content['field_cource_teacher']); ?>
						
						<?php if (isset($teacher_image_url)): ?>
							<div class="teacher-image"><img src="<?php print $teacher_image_url; ?>" /></div>
							
						<?php endif; ?>
						
						<?php if (isset($teacher_name)): ?>
							<h3><?php print $teacher_name; ?></h3>
							<div class="name-sub">Автор и ведущая программы</div>
						<?php endif; ?>
						
						<?php if (isset($teacher_description)): ?>
							<div class="teacher-decription"><?php print $teacher_description; ?></div>
						<?php endif; ?>
						
					</div>
				 <?php endif; ?>
				 
				 <?php //if (isset($content['field_cource_shedule'])): ?>
					<div class="schedule">
						<?php //print render($content['field_cource_shedule']); ?>
						<div class="shedule-title">Время и график занятий:</div>
						<?php if (isset($content['field_cource_shedule_days'])): ?>
							<?php print render($content['field_cource_shedule_days']); ?>
						<?php endif; ?>
            
            <?php if (isset($content['field_cource_place'])): ?>
              <div class="schedule schedule-place-val">        
                  <?php print render($content['field_cource_place']); ?>         
              </div>        
            <?php endif; ?>  
            
            
						<?php if (isset($content['field_cource_shedule_dates'])): ?>
							<?php print render($content['field_cource_shedule_dates']); ?>
						<?php endif; ?>
						<?php if (isset($content['field_cource_shedule_time'])): ?>
							<?php print render($content['field_cource_shedule_time']); ?>
						<?php endif; ?>
					</div>
				 <?php //endif; ?>
				 
				 <?php if (isset($content['field_cource_place'])): ?>
					<div class="schedule-place">
						<?php //print render($content['field_cource_place']); ?>
					</div>
				 <?php endif; ?>
         
         

				 <?php if (isset($content['field_cource_price'])): ?>
					 <div class="cost">
              <div class="cource_cost" style="display: none;" ><?php print $cource_cost; ?></div>
              <?php if (isset($is_action)): ?>
                <?php print render($content['field_cource_action_price']); ?>
                <?php else: ?>
                <?php print render($content['field_cource_price']); ?>
              <?php endif; ?>
					 </div>
				 <?php endif; ?>
         
         
          
				 <span class="cource-button field-content sign-up course-<?php print $node->nid; ?>">
					<?php busineschool_signup_include_modal();
					$apply_for_button = ctools_modal_text_button('<span class="ready-to-by">Хотите участвовать?</span>' . t('Apply for cource'), 'signup/nojs/action', t('Apply for cource'), 'ctools-modal-busineschool-dialog-signup-style');?>
				  <?php print $apply_for_button; ?>
				 </span>
				 
				 <span class="cource-button-buy sign-up course-<?php print $node->nid; ?>">				  
					<a class="buy-programm popup__link" data-title="<?php print $cource_title; ?>" data-price="<?php print $cource_cost; ?>" href="#"><span class="ready-to-by">Готовы сразу оплатить?</span>Купить программу</a>        
				 </span>
         
        <span class="cource-button-buy sign-up course-<?php print $node->nid; ?>">				  
					<a class="reserve-programm popup__link" data-title="<?php print $cource_title; ?>" data-price="2000" href="#"><span class="ready-to-by"></span>Забронировать место в группе</a>        
				</span>
				 
				 
				 
				</div>
				
        <!--
				<div class="col-1 mobile">
					
					<?php //if (isset($content['field_cource_description'])): ?>
						<?php //print render($content['field_cource_description']); ?>
					<?php //endif; ?>
					
					<?php //if (isset($content['field_cource_goal'])): ?>
						<?php //print render($content['field_cource_goal']); ?>
					<?php //endif; ?>
									
					<?php //if (isset($content['field_cource_image'])): ?>
						<?php //print render($content['field_cource_image']); ?>
					<?php //endif; ?>
				</div>
        -->
				
			</div>

	</div>
</div>