<div class="node-wrapper">
<div class="col-1">
	<?php //if ($cource_date):?>
		<!--<div class="date"><?php print $cource_date; ?></div> -->
	<?php //endif; ?>
	<h2 class="title"><?php print l($node->title, 'node/' . $node->nid); ?></h2>
	<?php if ($out_body):?>
		<div class="body"><?php print $out_body; ?></div>
	<?php endif; ?>
		
	<?php
		busineschool_signup_include_modal();
		$apply_for_button = ctools_modal_text_button('<span class="ready-to-by">Хотите участвовать?</span>'. t('Apply for cource'), 'signup/nojs/action', t('Apply for cource'), 'ctools-modal-busineschool-dialog-signup-style');
	?>
	<div class="field-content sign-up course-<?php print $node->nid; ?>"><?php print $apply_for_button; ?></div>
	
	<span class="cource-button-buy sign-up course-<?php print $node->nid; ?> desktop-button">				  
    <a class="buy-programm popup__link" data-title="<?php print $cource_title; ?>" data-price="<?php print $cource_cost; ?>" href="#"><span class="ready-to-by">Готовы сразу оплатить?</span>Купить программу</a>
  </span>
   <span class="cource-button-buy sign-up sign-up-reserve course-<?php print $node->nid; ?> desktop-button">				  
      <a class="reserve-programm popup__link" data-title="<?php print $cource_title; ?>" data-price="2000" href="#"><span class="ready-to-by"></span>Забронировать место в группе</a>        
  </span>
	
</div>


<div class="col-2">
 
	<div class="name-sub">Автор и ведущий программы</div>
	 
	<?php if (isset($teacher_image_url)): ?>
		<div class="teacher-image"><img src="<?php print $teacher_image_url?>" /></div>
		
	<?php endif; ?>
					
	<?php if (isset($teacher_name)): ?>
		<h3><?php print $teacher_name; ?></h3>

	<?php endif; ?>
 
 <div class="schedule">
				<?php //print render($content['field_cource_shedule']); ?>
				<div class="shedule-title">Время и график занятий:</div>
				<?php if (isset($content['field_cource_shedule_days'])): ?>
					<?php print render($content['field_cource_shedule_days']); ?>
				<?php endif; ?>
        
        
        <?php if (isset($content['field_cource_place'])): ?>
          <div class="schedule schedule-place-val">        
              <?php print render($content['field_cource_place']); ?>         
          </div>        
        <?php endif; ?>
        
				<?php if (isset($content['field_cource_shedule_dates'])): ?>
					<?php print render($content['field_cource_shedule_dates']); ?>
				<?php endif; ?>
				<?php if (isset($content['field_cource_shedule_time'])): ?>
					<?php print render($content['field_cource_shedule_time']); ?>
				<?php endif; ?>
			</div>
		 <?php //endif; ?>
		 
		
     

		 <?php if (isset($content['field_cource_price'])): ?>
			 <div class="cost">
          <div class="cource_cost cc" style="display: none;" ><?php print $cource_cost; ?></div>
          
          <?php //echo '<pre>'; print_r($content['field_cource_action_price']); echo '</pre>'; ?>
          
					 <?php if (isset($is_action)): ?>
            <?php print render($content['field_cource_action_price']); ?>
            <?php else: ?>
            <?php print render($content['field_cource_price']); ?>
          <?php endif; ?>
			 </div>
		 <?php endif; ?>
		 
		 <div class="field-content cource-button mobile sign-up course-<?php print $node->nid; ?>"><?php print $apply_for_button; ?></div>
		 
  <span class="cource-button-buy sign-up course-<?php print $node->nid; ?> mobile-button">				  
    <a class="buy-programm popup__link" data-title="<?php print $cource_title; ?>" data-price="<?php print $cource_cost; ?>" href="#"><span class="ready-to-by">Готовы сразу оплатить?</span>Купить программу</a>
  </span>
   <span class="cource-button-buy sign-up sign-up-reserve course-<?php print $node->nid; ?> mobile-button">				  
      <a class="reserve-programm popup__link" data-title="<?php print $cource_title; ?>" data-price="2000" href="#"><span class="ready-to-by"></span>Забронировать место в группе</a>        
  </span>
		  

</div>
</div>