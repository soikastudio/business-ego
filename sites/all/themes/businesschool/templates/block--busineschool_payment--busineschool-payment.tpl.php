<?php
/**
 * Created by PhpStorm.
 * User: kostya
 * Date: 13.05.16
 * Time: 18:54
 */ 
?>

<div id="payment_form" class="popup" style="display: none;">
  <button class="popup__close"></button>

  <div class="content">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <div class="popup__title"><?php print $block->subject ?></div>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <?php print $content ?>
  </div>

</div>


