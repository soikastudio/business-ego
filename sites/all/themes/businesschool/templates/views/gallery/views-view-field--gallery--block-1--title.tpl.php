<?php
	$image_count = 0;
  if (isset($row->_field_data['nid']['entity']->field_gallery_image['und'])) {
    $image_count = count($row->_field_data['nid']['entity']->field_gallery_image['und']);
  }
?>

<span class="image-count"><?php print $image_count; ?></span>
<?php print $output; ?>
