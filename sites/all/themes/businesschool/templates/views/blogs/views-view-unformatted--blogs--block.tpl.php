<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="swiper-wrapper"> 
	<?php foreach ($rows as $id => $row): ?>
		<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
			<?php print $row; ?>
		</div>
	<?php endforeach; ?>
 </div>
 <div id="blogs-pagination"></div>
 
 <div id="swiper-blogs-next"></div>
 <div id="swiper-blogs-prev"></div>