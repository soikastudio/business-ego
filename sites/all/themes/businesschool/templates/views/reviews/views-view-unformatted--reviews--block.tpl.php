<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>


<div class="review-image-wrapper">
	<img src="/sites/all/themes/businesschool/images/reviews-slider-image-wrapper.png">
</div> 

<div class="review-image-wrapper-top">
	<img src="/sites/all/themes/businesschool/images/reviews-slider-image-wrapper-top.png">
</div> 
<div class="swiper-wrapper"> 
	<?php foreach ($rows as $id => $row): ?>
		<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
			<?php print $row; ?>
		</div>
	<?php endforeach; ?>
 </div>
 <div id="reviews-pagination"></div>
 
 <div id="swiper-reviews-next"></div>
 <div id="swiper-reviews-prev"></div>
 
 <div class="reviews-ref"><a href="/reviews">Отзывы наших клиентов</a>
<a class="mobile-reviews" href="/reviews">Все отзывы</a></div>