<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php $row_count = 0; ?>
<div class="swiper-wrapper"> 
	<?php foreach ($rows as $id => $row): ?>
		<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
			<?php print $row; ?>			
		</div>
		<?php $row_count++; ?>
		<?php print $row_count % 3 == 0 ? '<div class="row-divider"><div class="line"></div></div>' : ''; ?>
	<?php endforeach; ?>
 </div>
 <div id="cources-pagination" class="cources-pagination"></div>
 
 <div id="swiper-cources-next" class="swiper-cources-next"></div>
 <div id="swiper-cources-prev" class="swiper-cources-prev"></div>
 