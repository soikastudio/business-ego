<?php

	$refs_array = array();
	
	if (isset($row->field_field_banner_ref)) {
		$count = 0;
		foreach ($row->field_field_banner_ref as $key => $ref_node) {
			if ($count <=1 ) {
				$refs_array[] = '<span class="ref-' . $key . '">' . l($ref_node['rendered']['#title'], 'node/' . $ref_node['raw']['nid']) .  '</span>';
			}			
			$count++;
		}
	}

?>

<?php foreach ($refs_array as $ref): ?>
	<?php print $ref;?>
<?php endforeach; ?>
