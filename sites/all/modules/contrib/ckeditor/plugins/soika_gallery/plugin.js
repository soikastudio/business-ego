/**
 * @file Plugin for inserting images with soika_gallery
 */
(function ($) {
  CKEDITOR.plugins.add('soika_gallery', {

    requires: [],

    init: function(editor) {

      //console.log('editor');
      // Add Button
      editor.ui.addButton('soika_gallery', {
        label: 'Soika gallery',
        command: 'soika_gallery',
        icon: this.path + 'soika_gallery.png'
      });
      // Add Command
      editor.addCommand('soika_gallery', {
        exec : function () {
          var path = (Drupal.settings.soika_gallery.url.wysiwyg_ckeditor) ? Drupal.settings.soika_gallery.url.wysiwyg_ckeditor : Drupal.settings.soika_gallery.url.ckeditor
          //var media = window.open(path, { 'opener' : window, 'editorname' : editor.name }, "dialogWidth:580px; dialogHeight:480px; center:yes; resizable:yes; help:no;");
        
           if (window.showModalDialog) {
            var media = window.showModalDialog(path, { 'opener' : window, 'editorname' : editor.name }, "dialogWidth:750px; dialogHeight:320px; center:yes; resizable:yes; help:no;");
          }
          else {
            var media = window.open(path + (path.indexOf('?') == -1 ? '?' : '&') + 'editorname='+encodeURI(editor.name), null, "width=750,height=320,resizable,alwaysRaised,dependent,toolbar=no,location=no,menubar=no");
          }
        
        }
      });

      // Register an extra fucntion, this will be used in the popup.
      editor._.soika_galleryFnNum = CKEDITOR.tools.addFunction(insert, editor);
    }

  });

  function insert(params, editor) {
    var selection = editor.getSelection(),
      ranges = selection.getRanges(),
      range,
      textNode;

    editor.fire('saveSnapshot');

    var str = '[gallery:' + params.nid;
    str += ']';

    for (var i = 0, len = ranges.length; i < len; i++) {
      range = ranges[i];
      range.deleteContents();

      textNode = CKEDITOR.dom.element.createFromHtml(str);
      range.insertNode(textNode);
    }

    range.moveToPosition(textNode, CKEDITOR.POSITION_AFTER_END);
    range.select();

    editor.fire('saveSnapshot');
  }

})(jQuery);
