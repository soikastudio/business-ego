/**
 * @file soika_gallery ckeditor dialog helper
 */

var soika_gallery_dialog = {};
(function ($) {
soika_gallery_dialog = {
  init : function() {
    //Get CKEDITOR
  //  CKEDITOR = dialogArguments.opener.CKEDITOR;
    //Get the current instance name
  //  var name = dialogArguments.editorname;
  
  //Get CKEDITOR and current instance name
    if (typeof(dialogArguments) != 'undefined') {
      CKEDITOR = dialogArguments.opener.CKEDITOR;
      var name = dialogArguments.editorname;
    }
    else {
      CKEDITOR = window.opener.CKEDITOR;
      var name = decodeURI((RegExp('editorname=(.+?)(&|$)').exec(location.search)||[,null])[1]);
    }

    //Get the editor instance
    editor = CKEDITOR.instances[name];
  },

  insert : function() {
    // Get the params from the form
    var params = this._getParams();
    //If no file url, just close this window
    if(params.nid == "") {
      window.close();
    }
    else {
      CKEDITOR.tools.callFunction(editor._.soika_galleryFnNum, params, editor);
      window.close();
    }
  },

  _getParams : function () {
    var params = {};
    $('fieldset:first-child input, fieldset:first-child select').each(function() {
      if($(this).attr('type') == "checkbox") {
        if($(this).is(':checked')) {
          params[$(this).attr('name')] = $(this).val();
        }
      }
      else {
        if($(this).val() != "" && $(this).val() != "none") {
          params[$(this).attr('name')] = $(this).val();
        }
      }
    });

    return params;  
  }
};

$(document).ready(function() {
  var CKEDITOR, editor;

  soika_gallery_dialog.init();

  $('#edit-insert').click(function() {
    soika_gallery_dialog.insert();
    return false;
  });

  $('#edit-cancel').click(function() {
    window.close();
    return false;
  });
});

})(jQuery);
