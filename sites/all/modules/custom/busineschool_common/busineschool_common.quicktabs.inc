<?php
/**
 * @file
 * busineschool_common.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function busineschool_common_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'cources_programm';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Программы обучения в Москве';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'views_delta_cources_section-block_2',
      'hide_title' => 1,
      'title' => 'Личные компетенции',
      'weight' => '-100',
      'type' => 'block',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Личные компетенции');
  t('Программы обучения в Москве');

  $export['cources_programm'] = $quicktabs;

  return $export;
}
