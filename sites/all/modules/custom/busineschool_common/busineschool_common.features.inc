<?php
/**
 * @file
 * busineschool_common.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function busineschool_common_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function busineschool_common_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function busineschool_common_image_default_styles() {
  $styles = array();

  // Exported image style: blog_preview.
  $styles['blog_preview'] = array(
    'label' => 'blog_preview',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 381,
          'height' => 230,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: gallery_album.
  $styles['gallery_album'] = array(
    'label' => 'gallery_album',
    'effects' => array(
      10 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 377,
          'height' => 230,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: gallery_full.
  $styles['gallery_full'] = array(
    'label' => 'gallery_full',
    'effects' => array(
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 795,
          'height' => 500,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: gallery_full_mobile.
  $styles['gallery_full_mobile'] = array(
    'label' => 'gallery_full_mobile',
    'effects' => array(
      12 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 272,
          'height' => 335,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: gallery_preview.
  $styles['gallery_preview'] = array(
    'label' => 'gallery_preview',
    'effects' => array(
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 248,
          'height' => 153,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: reviews_main.
  $styles['reviews_main'] = array(
    'label' => 'reviews_main',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 207,
          'height' => 207,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: reviews_preview.
  $styles['reviews_preview'] = array(
    'label' => 'reviews_preview',
    'effects' => array(
      7 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 500,
          'height' => 160,
          'anchor' => 'center-top',
        ),
        'weight' => 1,
      ),
      8 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 160,
          'height' => 160,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: teachers_preview_160_160.
  $styles['teachers_preview_160_160'] = array(
    'label' => 'teachers_preview_160_160',
    'effects' => array(
      13 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 160,
          'height' => 160,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function busineschool_common_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Статья'),
      'base' => 'node_content',
      'description' => t('Статья — это документ, который обычно используется для новостей, анонсов и любых других сообщений, для которых не задействованы другие типы документов. Этот тип документа также используют для ведения персонального блога. По умолчанию, документы этого типа выводятся на первую страницу сайта и их разрешено комментировать.'),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'banner' => array(
      'name' => t('Баннер'),
      'base' => 'node_content',
      'description' => t('Баннер, отображается в слайдере на главной'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'blog' => array(
      'name' => t('Блог'),
      'base' => 'node_content',
      'description' => t('Статья блога'),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'cource' => array(
      'name' => t('Программа'),
      'base' => 'node_content',
      'description' => t('Курс, программа'),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'gallery' => array(
      'name' => t('Галерея'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'review' => array(
      'name' => t('Отзывы'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'seminar' => array(
      'name' => t('Выездной семинар'),
      'base' => 'node_content',
      'description' => t('Выездной семинар'),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'teacher' => array(
      'name' => t('Преподаватель'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
