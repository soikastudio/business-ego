<?php
/**
 * @file
 * busineschool_common.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function busineschool_common_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Блок Руководитель на странице О нас';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'about_us_page_block';
  $fe_block_boxes->body = '<div class="about-us-leader">

<div class="about-us-leader-image"><img src="/sites/all/themes/businesschool/images/about-us-leader.png" /></div>
<div class="about-us-leader-info">
<div class="about-us-leader-info-name">Инга Юрова</div>

<div class="about-us-leader-info-appointment">Бизнес-тренер, психолог, генеральный директор компании «Бизнес-Эго»</div>

<div class="about-us-leader-info-text">Обучающими курсами для деловых людей<br class="hidden" />
<br />
я занимаюсь уже 8 лет, и вот что стало <br class="hidden" />
<br />
очевидным для меня за это время. <br class="hidden" />
<br />
<br class="hidden" />
<br class="hidden" />
<br />
<br /><br class="hidden" />
</div>
</div>


</div>
';

  $export['about_us_page_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Адрес обучения';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'address_footer';
  $fe_block_boxes->body = '<div style="width: 352px; height: 67px;">г. Москва, ул. Павловская, 18
<div class="requisites"><a href="/requisites">Платежные реквизиты компании</a></div>
</div>
';

  $export['address_footer'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Блок на странице контактов';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'contacts_page_block';
  $fe_block_boxes->body = '<div class="contacts-wrapper">

	<div class="contacts-wrapper-top">
		<div class="contacts-leader">

			<div class="contacts-leader-image"><image src="/sites/all/themes/businesschool/images/inga-contacts.jpg" /></div>

			<div class="contacts-leader-info">
				<div class="contacts-leader-info-name">Инга Юрова</div>
				<div class="contacts-leader-info-appointment"> Бизнес-тренер, консультирующий психолог и руководитель "Бизнес-Эго"</div>
				<div class="contacts-leader-info-phone">8-926-575-92-94</div>
				
			</div>
		 
		</div>
		
		<div class="contacts-block">

			<div class="contacts-find">
			
				<div class="contacts-find caption">Адрес обучения:</div>				
				<div class="contacts-find value">г. Москва, ул. Павловская, 18</div>

			</div>
			
			
			<div class="contacts-phone-email">

				<div class="contacts-phone-email caption">Контакты:</div>				
				<div class="contacts-phone-email value">
					<div class="contacts-phone value">8 (495) 003-27-87</div>
					<div class="contacts-email value">info@b-ego.ru</div>
				</div>
				
			</div>

		</div>
	</div>
	
	<div class="contacts-wrapper-bottom">
		<div class="contacts-map">
			<script type="text/javascript">   
				ymaps.ready(function () {
					var myMap = new ymaps.Map(\'contacts-map\', {
							center: [55.712464, 37.626344],
							zoom: 13
					}),
					myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: \'Наше расположение\'
					}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: \'default#image\',
							// Своё изображение иконки метки.
							iconImageHref: \'/sites/all/themes/businesschool/images/map-locator.png\',
							// Размеры метки.
							iconImageSize: [38, 54],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-17, -52]
					});
					myMap.geoObjects.add(myPlacemark);
				});      
			</script>
			<div id="contacts-map"></div>
	   
		</div>
	</div>
	

</div>';

  $export['contacts_page_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Сделано в студии';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'made_in_footer';
  $fe_block_boxes->body = '<p><a class="st-name" href="http://soika-studio.com">Сделано <span class="studio">в студии "Soika" </span></a></p>
';

  $export['made_in_footer'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Карта в футере';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'map_footer';
  $fe_block_boxes->body = '<script type="text/javascript">   
        ymaps.ready(function () {
          var myMap = new ymaps.Map(\'map\', {
              center: [55.712464, 37.626344],
              zoom: 15
          }),
          myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
              hintContent: \'Наше расположение\'
          }, {
              // Опции.
              // Необходимо указать данный тип макета.
              iconLayout: \'default#image\'
              // Своё изображение иконки метки.
              //iconImageHref: \'/sites/all/themes/znaksudby/images/inner/map-locator.png\',
              // Размеры метки.
              //iconImageSize: [38, 54],
              // Смещение левого верхнего угла иконки относительно
              // её "ножки" (точки привязки).
              //iconImageOffset: [-17, -52]
          });
          myMap.geoObjects.add(myPlacemark);
        });      
       </script>
    <div id="map"></div>
    <div id="map-overlay"></div>

';

  $export['map_footer'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Карта в контактах мобильный';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'mobile_contacts_map';
  $fe_block_boxes->body = '<div class="contacts-map-mobile">
	<script type="text/javascript">   
		ymaps.ready(function () {
			var myMap = new ymaps.Map(\'contacts-map-mobile\', {
					center: [57.13236, 65.60146],
					zoom: 12
			}),
			myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
					hintContent: \'Наше расположение\'
			}, {
					// Опции.
					// Необходимо указать данный тип макета.
					iconLayout: \'default#image\',
					// Своё изображение иконки метки.
					iconImageHref: \'/sites/all/themes/businesschool/images/map-locator.png\',
					// Размеры метки.
					iconImageSize: [38, 54],
					// Смещение левого верхнего угла иконки относительно
					// её "ножки" (точки привязки).
					iconImageOffset: [-17, -52]
			});
			myMap.geoObjects.add(myPlacemark);
		});      
	</script>
	<div id="contacts-map-mobile"></div>
 
</div>';

  $export['mobile_contacts_map'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Подать заявку на участие мобильный';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'mobile_sign_up';
  $fe_block_boxes->body = '<p><a href="/cources">Подать заявку на участие</a></p>
';

  $export['mobile_sign_up'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Телефон в футере';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'phone_footer';
  $fe_block_boxes->body = '<div class="phone-title">&nbsp;</div>

<div>(495)003-45-15</div>
';

  $export['phone_footer'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Блок на страницах программ и выездных семинаров';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'seo_programs';
  $fe_block_boxes->body = '<p>Описание</p>
';

  $export['seo_programs'] = $fe_block_boxes;

  return $export;
}
