<?php

  $custom_tabs = array(
		array('title' => 'Tab 1', 'contents' => $current_month ),
		array('title' => 'Tab 2', 'contents' => $next_month),
		array('title' => 'Tab 3', 'contents' => $third_month),
	);
	
	$quicktabs_options = array(
		'renderer' => 'quicktabs', 
		'hide_empty_tabs' => 0, 
		'ajax' => 1,
	);
	
	$count = 0; 
	$tabs_count = 1;
	foreach ($months as $key => $month) {
		$first_class = $tabs_count == 1 ? ' first' : ''; 
		$last_class = $tabs_count == 3 ? ' last' : '';
		$num = $month['num'] <=9  ?  '0' . $month['num'] : $month['num'];
		$custom_tabs[$count]['title'] = '<span class="month-name">' . $month['month_name'] .  '</span>' .  '<span class="num">' . $num .  '</span>';
		$tabs_count++;
		$count++;
	}
	
	$calendar_tabs = quicktabs_build_quicktabs('calendar_blocks', $quicktabs_options, $custom_tabs);
	
 ?>
 
 <?php print render($calendar_tabs); ?>