<?php
/**
 * @file
 * busineschool_common.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function busineschool_common_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cource_payment|node|cource|form';
  $field_group->group_name = 'group_cource_payment';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Стоимость и оплата',
    'weight' => '12',
    'children' => array(
      0 => 'field_cource_price',
      1 => 'field_cource_action_price',
      2 => 'field_cource_is_action',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cource-payment field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_cource_payment|node|cource|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cource_shedule|node|cource|form';
  $field_group->group_name = 'group_cource_shedule';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cource';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Время и график занятий',
    'weight' => '14',
    'children' => array(
      0 => 'field_cource_shedule_dates',
      1 => 'field_cource_shedule_days',
      2 => 'field_cource_shedule_time',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cource-shedule field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_cource_shedule|node|cource|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Время и график занятий');
  t('Стоимость и оплата');

  return $field_groups;
}
