<?php
/**
 * @file
 * busineschool_signup.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function busineschool_signup_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'applications';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Заявки на участие';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Заявки на участие';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Имя';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Адрес email  */
  $handler->display->display_options['fields']['field_application_email']['id'] = 'field_application_email';
  $handler->display->display_options['fields']['field_application_email']['table'] = 'field_data_field_application_email';
  $handler->display->display_options['fields']['field_application_email']['field'] = 'field_application_email';
  /* Field: Content: Название города  */
  $handler->display->display_options['fields']['field_application_city']['id'] = 'field_application_city';
  $handler->display->display_options['fields']['field_application_city']['table'] = 'field_data_field_application_city';
  $handler->display->display_options['fields']['field_application_city']['field'] = 'field_application_city';
  /* Field: Content: Название курса */
  $handler->display->display_options['fields']['field_application_cource']['id'] = 'field_application_cource';
  $handler->display->display_options['fields']['field_application_cource']['table'] = 'field_data_field_application_cource';
  $handler->display->display_options['fields']['field_application_cource']['field'] = 'field_application_cource';
  /* Field: Content: Номер телефона */
  $handler->display->display_options['fields']['field_application_phone']['id'] = 'field_application_phone';
  $handler->display->display_options['fields']['field_application_phone']['table'] = 'field_data_field_application_phone';
  $handler->display->display_options['fields']['field_application_phone']['field'] = 'field_application_phone';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'application_form' => 'application_form',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/content/applications';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Заявки на участие';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['applications'] = array(
    t('Master'),
    t('Заявки на участие'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Имя'),
    t('Адрес email '),
    t('Название города '),
    t('Название курса'),
    t('Номер телефона'),
    t('Page'),
  );
  $export['applications'] = $view;

  return $export;
}
