<?php
/**
 * @file
 * busineschool_signup.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function busineschool_signup_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function busineschool_signup_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function busineschool_signup_node_info() {
  $items = array(
    'application_form' => array(
      'name' => t('Заявка на участие'),
      'base' => 'node_content',
      'description' => t('Тип данных используется для вывода формы заявки на участие'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
