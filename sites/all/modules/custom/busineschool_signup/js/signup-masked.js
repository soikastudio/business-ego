(function ($) {
  
  Drupal.behaviors.maskedFormBlock = {
    attach: function (context, settings) {

      var phone_field_signup =  $(".block-busineschool-signup .field-name-field-application-phone input");            
			if (phone_field_signup) {
        $(".block-busineschool-signup .field-name-field-application-phone input").mask("+7(999) 999-9999");
      }
          
    }
  }
  
  
})(jQuery);