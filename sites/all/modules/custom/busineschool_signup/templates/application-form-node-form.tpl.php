<?php
// Render or hide parts of $form: var_export($form);
// Example given:
// hide($form['title']);
// print render($form['first']);
// Render remaining form elements as usual.
// print drupal_render_children($f

  hide($form['menu']);
  hide($form['path']);
  hide($form['comment_settings']);
  hide($form['additional_settings']);

?>

<div id = "signup-form-top"></div>

<div class="<?php print $class_contact_form; ?>">
 
    <div class="<?php print $class_form_title; ?> form__title">Оставить заявку на участие в программе</div>
    <div class="col-1">
      <div class="form__block name">
        <!-- <label class="form__label">Представьтесь</label> -->
        <?php print drupal_render($form['title']); ?>
      </div>
			
			<div class="form__block age">
        <!-- <label class="form__label">Представьтесь</label> -->
        <?php print drupal_render($form['field_application_age']); ?>
      </div>
			
		
    </div>
    <div class="col-2">
      
				<div class="form__block phone">
			  <!--<label class="form__label">Телефон</label>-->
				<?php print drupal_render($form['field_application_phone']); ?>
			</div>
			
			
      <div class="form__block email">
       <!-- <label class="form__label">Ваш e-mail</label> -->
        <?php print drupal_render($form['field_application_email']); ?>
      </div>
			
			<div class="form__block city">
			  <!--<label class="form__label">Телефон</label>-->
				<?php print drupal_render($form['field_application_city']); ?>
			</div>
    </div>
		
		<div class="form__cource">
			  <!--<label class="form__label">Телефон</label>-->
				<?php print drupal_render($form['field_application_cource']); ?>
			</div>
      
      	<div class="form__block terms">
			  <!--<label class="form__label">Телефон</label>-->
				<?php print drupal_render($form['field_terms_of_use']); ?>
			</div>

  <?php print drupal_render_children($form); ?>
</div>
