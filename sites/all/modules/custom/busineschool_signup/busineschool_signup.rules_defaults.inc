<?php
/**
 * @file
 * busineschool_signup.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function busineschool_signup_default_rules_configuration() {
  $items = array();
  $items['rules__email_'] = entity_import('rules_config', '{ "rules__email_" : {
      "LABEL" : "\\u041e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043a\\u0430 email - \\u043f\\u043e\\u0441\\u043b\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u0438\\u044f \\u0444\\u043e\\u0440\\u043c\\u044b \\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0430\\u044f \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "busineschool_contacts" ],
      "ON" : { "callme_submit" : [] },
      "DO" : [
        { "mail" : {
            "to" : "ladovod@gmail.com",
            "subject" : "\\u041e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0430 \\u0444\\u043e\\u0440\\u043c\\u0430 \\u0022\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u044c \\u043c\\u043d\\u0435\\u0022",
            "message" : "\\u041e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043a\\u0430 \\u0444\\u043e\\u0440\\u043c\\u044b \\u0022\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0430\\u044f \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f\\u0022\\r\\n\\u0418\\u043c\\u044f: [name:value]\\r\\nEmail: [email:value]\\r\\n\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d: [phone:value]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_send_email_writeme'] = entity_import('rules_config', '{ "rules_send_email_writeme" : {
      "LABEL" : "\\u041e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043a\\u0430 email - \\u043f\\u043e\\u0441\\u043b\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u0438\\u044f \\u0444\\u043e\\u0440\\u043c\\u044b \\u041d\\u0430\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "busineschool_contacts" ],
      "ON" : { "writeme_submit" : [] },
      "DO" : [
        { "mail" : {
            "to" : "ladovod@gmail.com",
            "subject" : "\\u041e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0430 \\u0444\\u043e\\u0440\\u043c\\u0430 \\u0022\\u041d\\u0430\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u043c\\u043d\\u0435\\u0022",
            "message" : "\\u041e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043a\\u0430 \\u0444\\u043e\\u0440\\u043c\\u044b \\u0022\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441\\u043e \\u043c\\u043d\\u043e\\u0439\\u0022\\r\\n\\u0418\\u043c\\u044f: [name:value]\\r\\nEmail: [email:value]\\r\\n\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d: [phone:value]",
            "from" : "[email:value]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_user_added_application'] = entity_import('rules_config', '{ "rules_user_added_application" : {
      "LABEL" : "\\u041f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u044c \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u043b \\u0437\\u0430\\u044f\\u0432\\u043a\\u0443 \\u043d\\u0430 \\u0443\\u0447\\u0430\\u0441\\u0442\\u0438\\u0435",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--application_form" : { "bundle" : "application_form" } },
      "DO" : [
        { "mail" : {
            "to" : "ladovod@gmail.com",
            "subject" : "\\u0417\\u0430\\u044f\\u0432\\u043a\\u0430 \\u043d\\u0430 \\u043a\\u0443\\u0440\\u0441",
            "message" : "\\u0418\\u043c\\u044f: [node:title]\\r\\nEmail: [node:field-application-email]\\r\\n\\u0412\\u043e\\u0437\\u0440\\u0430\\u0441\\u0442: [node:field-application-age]\\r\\n\\u0422\\u0435\\u043b.: [node:field-application-phone]\\r\\n\\u0413\\u043e\\u0440\\u043e\\u0434: [node:field-application-city]\\r\\n\\u041a\\u0443\\u0440\\u0441 [node:field-application-cource]",
            "from" : "[node:field_application_email]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
